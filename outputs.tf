output "alb_dns_name" {
  description = "Load balancer URL to access the application."
  value       = module.ecs.alb_dns_name
}

output "vpc_id" {
  description = "ID output of the vpc."
  value       = module.vpc.vpc_id
}