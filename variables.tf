# VPC variables
variable "vpc_name" {
  description = "The name of the vpc will be used with ECS."
  type        = string
  default     = "ecs-vpc"
}

variable "vpc_cidr" {
  description = "Network CIDR will be used."
  type        = string
  default     = "10.0.0.0/16"
}

variable "availability_zones" {
  description = "Availability zones in where the subnets will be."
  type        = list(any)
  default     = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "private_subnets" {
  description = "Private subnets CIDRs."
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets" {
  description = "Public subnets CIDRs."
  type        = list(any)
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

#ECS variables
variable "service_name" {
  description = "Name of the ECS service."
  type        = string
  default     = "ecs-service"
}

variable "docker_tag" {
  description = "Tag of the docker image."
  type        = string
  default     = "latest"
}

variable "image" {
  description = "Name of the docker image."
  type        = string
  default     = "flask-app"
}

variable "ecr_repo_name" {
  description = "URL of the repo that holds the image."
  type        = string
  default     = "010747459958.dkr.ecr.eu-west-1.amazonaws.com"
}

variable "desired_count" {
  description = "Number of instance for the ECS service."
  type        = number
  default     = 2
}

variable "launch_type" {
  description = "Launch type of the ECS service."
  type        = string
  default     = "FARGATE"
}

variable "container_port" {
  description = "Name of port running in the container."
  type        = number
  default     = 5000
}

variable "container_name" {
  description = "Name of the running container in ECS."
  type        = string
  default     = "flask-app"
}

variable "cpu" {
  description = "Amoud of CPU needed for the container."
  type        = number
  default     = 1024
}

variable "memory" {
  description = "Amount of memory needed for the container."
  type        = number
  default     = 2048
}

variable "protocol" {
  description = "Which protocol to use."
  type        = string
  default     = "tcp"
}

variable "region" {
  description = "Region in where the container will run."
  type        = string
  default     = "eu-west-1"
}

variable "network_mode" {
  description = "Network mode of the task defination.."
  type        = string
  default     = "awsvpc"
}

variable "scaling" {
  description = "Scaling options for auto scalling the application."
  type        = map(any)
  default = {
    min_capacity = 3
    max_capacity = 4
    metric       = "memory"
    threshold    = 80
  }
}
