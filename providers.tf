provider "aws" {
  region = "eu-west-1"
  default_tags {
    tags = {
      Environment = "dev"
      Terraform   = "true"
    }
  }
}
