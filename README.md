# ecs-terraform

## Getting started
This is a terraform module that runs a simple flask app on an internet facing ECS service behind a public load balancer that is running on 3 availability zones. 
The module uses the official aws vpc module to create a vpc, three public subnets and three private subnets.
ECS cluster and ECS service is running on that cluster with three instances for high availability, each in different availability.
The load balancer is accepting traffic on port 80 and then forwards the traffic to the running container on ECS which is running on port 5000.
Auto app scalling is set for the ECS service to scale the app if the memory exceeds 80% of the usage.

## Requirements
- Terraform >= 1.0.0
- go >= 1.18
- AWS account & AWS credentials stored in gitlab CI variables.

## Main compenents
- VPC.
- ECS.
- Application load balancer.
- CloudWatch log group.

## Docker
The image used is based on `python:3.12-rc-alpine3.17` to start a flask server that listens on port 5000 to host a simple `index.html` page.
The image is hosted on my ECR repo named flask-app.

## Usage
### Manual
1. Initialize terraform
```bash
terraform init
```
2. Execute terraform.
```bash
terraform apply --auto-approve --var-file=./dev.tfvars
```
3. Check the loadbalancer URL output from terraform and you should be able to access the flask app.
```bash
terraform output
```

### CI/CD
Gitlab CI is used to run multiple tests on the module.
- validate (to validate the terraform code)
- lint (to check if there is any linting issues)
- plan (to check if there is any errors with the plan)
- test (to check that the infrastructure will run with no issues and the loadbalancer URL returns 200 )

## Test
Test is done using terratest with Go to check that the load balancer URL will return 200 when accessed.

## Logs
Cloudwatch log group instance is created to store the logs of the running service named `ecs-log`.
