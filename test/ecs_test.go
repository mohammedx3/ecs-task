package test

// Needed modules to our code.
import (
	"testing"
	"fmt"
	"time"
	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/terraform"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"
)

// Check that the load balancer URL returns status of 200 when trying to access it.

func TestTerraformDeployExample(t *testing.T) {
	// tests running in parallel
	t.Parallel()

	// The folder where we have our Terraform code
	workingDir := "../"

	// At the end of the test, clean up all the resources we created
	defer test_structure.RunTestStage(t, "teardown", func() {
		terraformOptions := test_structure.LoadTerraformOptions(t, workingDir)
		terraform.Destroy(t, terraformOptions)
	})

	// The region AWS will perform all its deployment.
	test_structure.RunTestStage(t, "pick_region", func() {
		awsRegion := "eu-west-1"
		// Save the region, so that we reuse the same region when we skip stages.
		test_structure.SaveString(t, workingDir, "region", awsRegion)
	})

	// Deploy the web app.
	test_structure.RunTestStage(t, "deploy_initial", func() {
		awsRegion := test_structure.LoadString(t, workingDir, "region")
		initialDeploy(t, awsRegion, workingDir)
	})

	// Validate that the load balancer is deployed and is responding to HTTP requests.
	test_structure.RunTestStage(t, "validate_initial", func() {
		awsRegion := test_structure.LoadString(t, workingDir, "region")
		validateRunningLB(t, awsRegion, workingDir)
	})

}

// Do the initial deployment of the terraform configs.
func initialDeploy(t *testing.T, awsRegion string, workingDir string) {
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: workingDir,
	})

	// Save the Terraform Options struct so future test stages can use it
	test_structure.SaveTerraformOptions(t, workingDir, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)
}

// Validate the load balancer URL is reachable.
func validateRunningLB(t *testing.T, awsRegion string, workingDir string) {
	// Load the Terraform Options saved by the earlier deploy_terraform stage
	terraformOptions := test_structure.LoadTerraformOptions(t, workingDir)
	
	albDNSName := terraform.Output(t, terraformOptions, "alb_dns_name")
	endpoint := fmt.Sprintf("http://%s", albDNSName)
	retries := 30
	sleep := 5 * time.Second

	http_helper.HttpGetWithRetryWithCustomValidation(
		t,
		endpoint,
		nil,
		retries,
		sleep,
		func(statusCode int, body string) bool {
			return statusCode == 200
		},
	)
}
