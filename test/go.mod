module test

go 1.13

require (
	github.com/gruntwork-io/terratest v0.41.7
	github.com/stretchr/testify v1.8.1
	golang.org/x/tools v0.4.0 // indirect
)
