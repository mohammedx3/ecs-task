module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name            = var.vpc_name
  cidr            = var.vpc_cidr
  azs             = var.availability_zones
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_nat_gateway = true
}

module "ecs" {
  source = "./modules/ecs"

  vpc_id          = module.vpc.vpc_id
  public_subnets  = module.vpc.public_subnets
  private_subnets = module.vpc.private_subnets

  service_name   = var.service_name
  docker_tag     = var.docker_tag
  image          = var.image
  ecr_repo_name  = var.ecr_repo_name
  desired_count  = var.desired_count
  launch_type    = var.launch_type
  container_port = var.container_port
  container_name = var.container_name
  cpu            = var.cpu
  memory         = var.memory
  protocol       = var.protocol
  region         = var.region
  network_mode   = var.network_mode
  scaling        = var.scaling
}