// ---------------------------------- ECS VARIABLES ----------------------------------
variable "service_name" {
  description = "Name of the ECS service."
  type        = string
}

variable "docker_tag" {
  description = "Tag of the docker image."
  type        = string
}

variable "image" {
  description = "Name of the docker image."
  type        = string
}

variable "ecr_repo_name" {
  description = "URL of the repo that holds the image."
  type        = string
}

variable "desired_count" {
  description = "Number of instance for the ECS service."
  type        = number
  default     = 3
}

variable "deployment_maximum_percent" {
  description = "Number of instance for the ECS service."
  type        = number
  default     = 200
}

variable "deployment_minimum_healthy_percent" {
  description = "Number of instance for the ECS service."
  type        = number
  default     = 75
}

variable "launch_type" {
  description = "Launch type of the ECS service."
  type        = string
  default     = "FARGATE"
}

variable "container_port" {
  description = "Name of port running in the container."
  type        = number
}

variable "container_name" {
  description = "Name of the running container in ECS."
  type        = string
}

variable "ecs_security_group" {
  description = "Name of the security group for ECS."
  type        = string
  default     = "ecs-sg"
}

variable "cpu" {
  description = "Amoud of CPU needed for the container."
  type        = number
}

variable "memory" {
  description = "Amount of memory needed for the container."
  type        = number
}

variable "protocol" {
  description = "Which protocol to use."
  type        = string
}

variable "region" {
  description = "Region in where the container will run."
  type        = string
}

variable "network_mode" {
  description = "Network mode of the task defination.."
  type        = string
}

variable "ecs_cluster_name" {
  description = "Name of the ECS cluster."
  type        = string
  default     = "ecs-cluster"
}

variable "scaling" {
  description = "Scaling options for auto scalling the application."
  type        = map(any)
}

// ---------------------------------- VPC VARIABLES ----------------------------------
variable "vpc_id" {
  description = "ID of the VPC where the security groups be created."
  type        = string
  default     = ""
}

variable "private_subnets" {
  description = "List of private subnets cidrs."
  type        = list(any)
  default     = [""]
}

variable "public_subnets" {
  description = "List of public subnets cidrs"
  type        = list(any)
  default     = [""]
}

// ---------------------------------- INSTANCE VARIABLES ----------------------------------
variable "instance_name" {
  description = "The names for the ASG and other resources in this module"
  type        = string
  default     = "ecs-alb"
}

variable "instance_port" {
  description = "The port each EC2 Instance should listen on for HTTP requests."
  type        = number
  default     = 80
}

variable "alb_port" {
  description = "The port the ALB should listen on for HTTP requests"
  type        = number
  default     = 80
}

variable "instance_type" {
  description = "The EC2 instance type to run."
  type        = string
  default     = "t2.micro"
}

variable "aws_cloudwatch_log_group" {
  description = "Name of the cloudwatch log group."
  type        = string
  default     = "ecs-log"
}
