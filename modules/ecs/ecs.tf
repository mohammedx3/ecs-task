data "aws_iam_role" "ecs_task_execution_role" {
  name = "ecsTaskExecutionRole"
}

resource "aws_ecs_cluster" "cluster" {
  name = var.ecs_cluster_name
}

resource "aws_cloudwatch_log_group" "this" {
  name = var.aws_cloudwatch_log_group
}

// ECS service with the needed settings to run the application. 
resource "aws_ecs_service" "service" {
  name          = var.service_name
  cluster       = aws_ecs_cluster.cluster.arn
  launch_type   = var.launch_type
  desired_count = var.desired_count

  network_configuration {
    security_groups = ["${aws_security_group.ecs.id}"]
    subnets         = var.private_subnets
  }

  task_definition = aws_ecs_task_definition.this.arn

  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent

  load_balancer {
    target_group_arn = aws_alb_target_group.alb_target_group.arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  depends_on = [aws_alb_listener.http]
}

// Security group for ECS to communiate with the load balancer.
resource "aws_security_group" "ecs" {
  name        = var.ecs_security_group
  description = "Security group for ECS to communicate with the load balancer."
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.alb.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "container_definition" {
  template = file("${path.module}/container_definition.json")
  vars = {
    image          = var.image
    ecr_repo_name  = var.ecr_repo_name
    docker_tag     = var.docker_tag
    name           = var.container_name
    cpu            = var.cpu
    memory         = var.memory
    port           = var.container_port
    protocol       = var.protocol
    log_group_name = aws_cloudwatch_log_group.this.name
    region         = var.region
  }
}

// Task defination to run the container.
resource "aws_ecs_task_definition" "this" {
  family                   = "task"
  container_definitions    = data.template_file.container_definition.rendered
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = var.network_mode
  cpu                      = var.cpu
  memory                   = var.memory

  depends_on = [aws_cloudwatch_log_group.this]
}

resource "aws_appautoscaling_target" "this" {
  max_capacity       = var.scaling["max_capacity"]
  min_capacity       = var.scaling["min_capacity"]
  resource_id        = "service/${aws_ecs_cluster.cluster.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "this" {
  name               = "${var.scaling["metric"]}-scaling-policy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${aws_ecs_cluster.cluster.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = var.scaling["metric"] == "memory" ? "ECSServiceAverageMemoryUtilization" : "ECSServiceAverageCPUUtilization"
    }

    scale_in_cooldown  = lookup(var.scaling, "scale_in_cooldown", 0)
    scale_out_cooldown = lookup(var.scaling, "scale_out_cooldown", 0)
    target_value       = var.scaling["threshold"]
  }

  depends_on = [aws_appautoscaling_target.this]
}
