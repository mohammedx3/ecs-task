// Security group for the load balancer.
resource "aws_security_group" "alb" {
  name        = "${var.instance_name}-sg"
  vpc_id      = var.vpc_id
  description = "Security group for the load balancer."
}

// Allow traffic on port 80.
resource "aws_security_group_rule" "alb_allow_http_inbound" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.alb.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow port 80 on the load balancer."
}

// Allowing all traffic coming out of the load balancer.
resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.alb.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow all outbound of the load balancer."
}

// Creating load balancer to distribute traffic to target group.
resource "aws_alb" "alb" {
  name            = var.instance_name
  security_groups = [aws_security_group.alb.id]
  subnets         = var.public_subnets
  lifecycle {
    create_before_destroy = true
  }
}

// Creating a load balancer listener on HTTP.
resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group.arn
  }

  lifecycle {
    create_before_destroy = true
  }
}

// ALB target group.
resource "aws_alb_target_group" "alb_target_group" {
  depends_on = [aws_alb.alb]

  name        = var.instance_name
  port        = var.instance_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  // Health check intervals on the instances.
  deregistration_delay = 10

  health_check {
    path                = "/"
    interval            = 15
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  lifecycle {
    create_before_destroy = true
  }
}

// The ALB listener rule will forward all requests to the target group.
resource "aws_alb_listener_rule" "listener_rule" {
  listener_arn = aws_alb_listener.http.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["*"]
    }
  }
}
